#include "gameobject.hpp"
#include <iostream>

using namespace std;

GameObject::GameObject(){
    positionY = 0;
    positionX = 0;
    sprite = 'c';

}

GameObject::GameObject(int positionX, int positionY, char sprite){
    setPositionX(positionX);
    setPositionY(positionY);
    setSprite(sprite);

}

int GameObject::getPositionX(){
    return positionX;

}
void GameObject::setPositionX(int positionX){
    this -> positionX = positionX;

}
int GameObject::getPositionY(){
    return positionY;

}
void GameObject::setPositionY(int positionY){
    this -> positionY = positionY;

}
char GameObject::getSprite(){
    return sprite;

}
void GameObject::setSprite(char sprite){
    this -> sprite = sprite;

}
