#ifndef GAMEOBJECT_HPP
#define GAMEOBJECT_HPP

using namespace std;

class GameObject{
    private:
        int positionX;
        int positionY;
        char sprite;

    public:
        GameObject();
        GameObject(int positionX, int positionY, char sprite);
        ~GameObject();
        char getSprite();
        void setSprite(char sprite);
        int getPositionX();
        void setPositionX(int positionX);
        int getPositionY();
        void setPositionY(int positionY);


};

#endif
